import 'package:flutter/material.dart';
import 'package:the_movie_db/common/app_images.dart';
import 'package:the_movie_db/ui/components/app_image_asset.dart';
import 'package:the_movie_db/ui/pages/example_page/example_page.dart';
import 'package:the_movie_db/ui/pages/list_movie_page/list_movie_page.dart';

class AppConfig {
  static String baseUrl = 'https://api.themoviedb.org';
  static String apiKey = '6364375763d65d5b6ec4983224bfc4bb';

  static String imageBackgroundDefault =
      'https://scontent.fhan17-1.fna.fbcdn.net/v/t39.30808-6/300370141_445684364259165_8893097931255509122_n.png?_nc_cat=111&ccb=1-7&_nc_sid=09cbfe&_nc_ohc=nZar8u6xwmAAX_3I45t&_nc_ht=scontent.fhan17-1.fna&oh=00_AfCU2Ia2RyURMrjPuWH05PvuRL0da7jkq3_lKnscPvbeOQ&oe=63E115CF';

  static final listItemBottomNavigationBar = <Widget>[
    AppImageAsset(
      imagePath: AppImages.icHome,
      color: Colors.black,
    ),
    AppImageAsset(
      imagePath: AppImages.icFavorite,
      color: Colors.black,
    ),
    AppImageAsset(
      imagePath: AppImages.icTicket,
      color: Colors.black,
    ),
    AppImageAsset(
      imagePath: AppImages.icAccount,
      color: Colors.black,
    ),
    AppImageAsset(
      imagePath: AppImages.icShuffle,
      color: Colors.black,
    ),
  ];

  static final listItemPageView = [
    const Center(
      child: ListMoviePage(),
    ),
    const Center(
      child: ExamplePage(
        text: "Page 2",
      ),
    ),
    const Center(
      child: ExamplePage(
        text: "Page 3",
      ),
    ),
    const Center(
      child: ExamplePage(
        text: "Page 4",
      ),
    ),
    const Center(
      child: ExamplePage(
        text: "Page 5",
      ),
    )
  ];
}
