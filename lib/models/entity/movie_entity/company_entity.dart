import 'package:json_annotation/json_annotation.dart';

part 'company_entity.g.dart';

@JsonSerializable()
class CompanyEntity {
  @JsonKey(name: 'id')
  final int? id;

  @JsonKey(name: 'logo_path')
  final String? logoPath;

  @JsonKey(name: 'name')
  final String? name;

  @JsonKey(name: 'origin_country')
  final String? originCountry;

  CompanyEntity({
    this.id,
    this.logoPath,
    this.name,
    this.originCountry,
  });

  factory CompanyEntity.fromJson(Map<String, dynamic> json) =>
      _$CompanyEntityFromJson(json);

  Map<String, dynamic> toJson() => _$CompanyEntityToJson(this);
}
