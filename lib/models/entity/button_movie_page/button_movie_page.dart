class ButtonMoviePage {
  final String image;
  final String buttonName;

  ButtonMoviePage(
    this.image,
    this.buttonName,
  );
}
