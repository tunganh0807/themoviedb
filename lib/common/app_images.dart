class AppImages {
  static String icGrid = 'assets/images/ic_grid.png';
  static String icTvSeries = 'assets/images/ic_tv_series.png';
  static String icMovieRoll = 'assets/images/ic_movie_roll.png';
  static String icCinema = 'assets/images/ic_cinema.png';
  static String icHome = 'assets/images/ic_home.png';
  static String icFavorite = 'assets/images/ic_favorite.png';
  static String icTicket = 'assets/images/ic_ticket.png';
  static String icAccount = 'assets/images/ic_account.png';
  static String icShuffle = 'assets/images/ic_shuffle.png';
  static String icBack = 'assets/images/ic_back.png';
  static String icShare = 'assets/images/ic_share.png';
  static String icBell = 'assets/images/ic_bell.png';
  static String icSearch = 'assets/images/ic_search.png';
  static String icMicro = 'assets/images/ic_micro.png';

}