import 'dart:ui';

class AppColors {
  static const List<Color> colorBackground = [
    Color(0xFF2B5876),
    Color(0xFF4E4376),
  ];

  static const List<Color> colorBackgroundSearchBar = [
    Color(0x4D6B66A6),
    Color(0x4D75D1DD),
  ];

  static const List<Color> colorBackgroundGenre = [
    Color(0x4DA6A1E0),
    Color(0x4DA1F3FE)
  ];

  static const List<Color> colorBackgroundButtonMoviePage = [
    Color(0x4DA6A1E0),
    Color(0x4DA1F3FE),
    Color(0x33FFFFFF),
  ];
  static const Color colorBorderSearchBar = Color(0x33FFFFFF);
  static const Color colorBackgroundBottomNavigationBar = Color(0xFF2B5876);
  static const Color colorYellow = Color(0xFFF5C518);
}
