import 'package:the_movie_db/common/app_images.dart';
import 'package:the_movie_db/models/entity/button_movie_page/button_movie_page.dart';

class AppButtonListMoviePage {
  static final List<ButtonMoviePage> listButton = [
    ButtonMoviePage(AppImages.icGrid, 'Genres',),
    ButtonMoviePage(AppImages.icTvSeries, 'TV series',),
    ButtonMoviePage(AppImages.icMovieRoll, 'Movies',),
    ButtonMoviePage(AppImages.icCinema, 'In Theatre',),
  ];
}
