import 'package:flutter/material.dart';

class AppTextStyle {
  static const white = TextStyle(fontFamily: 'BeVietNamPro', color: Colors.white,);
  static const blue = TextStyle(fontFamily: 'BeVietNamPro', color: Colors.blue,);
  static const black = TextStyle(fontFamily: 'BeVietNamPro', color: Colors.black,);

  static final whiteS18 = white.copyWith(fontSize: 18);
  static final whiteS12 = white.copyWith(fontSize: 12);

  static final whiteS18W700 = white.copyWith(fontWeight: FontWeight.w700,fontSize: 18);
  static final whiteS18W500 = white.copyWith(fontWeight: FontWeight.w500,fontSize: 18);
  static final whiteS18W400 = white.copyWith(fontWeight: FontWeight.w400,fontSize: 18);
  static final whiteS12W500 = white.copyWith(fontWeight: FontWeight.w500,fontSize: 12);
  static final whiteS12W700 = white.copyWith(fontWeight: FontWeight.w700,fontSize: 12);
  static final whiteS15W500 = white.copyWith(fontWeight: FontWeight.w500,fontSize: 15);
  static final whiteS9W400 = white.copyWith(fontWeight: FontWeight.w400,fontSize: 9);


  static final blueS12W500 = blue.copyWith(fontWeight: FontWeight.w500,fontSize: 12);

  static final blackS12W700 = black.copyWith(fontWeight: FontWeight.w700,fontSize: 12);

}
