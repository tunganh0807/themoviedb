import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:the_movie_db/models/entity/movie_entity/most_popular_movie_entity.dart';
import 'package:the_movie_db/models/entity/movie_entity/upcoming_movie_entity.dart';
import 'package:the_movie_db/models/enum/status_enum.dart';
import 'package:the_movie_db/repositories/movies_repository.dart';

part 'list_movie_state.dart';

class ListMovieCubit extends Cubit<ListMovieState> {
  MoviesRepository moviesRepo;

  ListMovieCubit({
    required this.moviesRepo,
  }) : super(const ListMovieState());

  void switchMostPopularDotIndicators(int index) {
    emit(
      state.copyWith(
        selectedIndexMostPopular: index,
      ),
    );
  }

  void switchUpcomingDotIndicators(int index) {
    emit(
      state.copyWith(
        selectedIndexUpcoming: index,
      ),
    );
  }

  void initData() {
    getListUpcomingMovie();
    getListPopularMovie();
  }

  Future<void> getListPopularMovie() async {
    try {
      emit(
        state.copyWith(
          loadPopularMovieStatus: StatusEnum.loading,
        ),
      );
      final result = await moviesRepo.getPopularMovies(page: 1);
      emit(
        state.copyWith(
          loadPopularMovieStatus: StatusEnum.success,
          mostPopularMovieEntity: result,
        ),
      );
    } catch (e) {
      emit(
        state.copyWith(
          loadPopularMovieStatus: StatusEnum.fail,
        ),
      );
      debugPrint(e.toString());
    }
  }

  Future<void> getListUpcomingMovie() async {
    try {
      emit(
        state.copyWith(
          loadUpcomingMovieStatus: StatusEnum.loading,
        ),
      );
      final result = await moviesRepo.getUpcomingMovies(page: 1);
      emit(
        state.copyWith(
          loadUpcomingMovieStatus: StatusEnum.success,
          upcomingMovieEntity: result,
        ),
      );
    } catch (e) {
      emit(
        state.copyWith(
          loadUpcomingMovieStatus: StatusEnum.fail,
        ),
      );
      debugPrint(e.toString());
    }
  }
}
