import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:the_movie_db/common/app_colors.dart';
import 'package:the_movie_db/common/app_text_style.dart';
import 'package:the_movie_db/models/entity/movie_entity/most_popular_movie_entity.dart';
import 'package:the_movie_db/models/entity/movie_entity/upcoming_movie_entity.dart';
import 'package:the_movie_db/models/enum/status_enum.dart';
import 'package:the_movie_db/repositories/movies_repository.dart';
import 'package:the_movie_db/ui/components/app_circular_progress_indicator.dart';
import 'package:the_movie_db/ui/components/text_widget.dart';
import 'package:the_movie_db/ui/pages/list_movie_page/list_movie_cubit.dart';
import 'package:the_movie_db/ui/pages/list_movie_page/widgets/list_button_app_widget.dart';
import 'package:the_movie_db/ui/pages/list_movie_page/widgets/most_popular_movie_widget.dart';
import 'package:the_movie_db/ui/pages/list_movie_page/widgets/notifi_widget.dart';
import 'package:the_movie_db/ui/pages/list_movie_page/widgets/search_bar_widget.dart';
import 'package:the_movie_db/ui/pages/list_movie_page/widgets/upcoming_movie_widget.dart';

class ListMoviePage extends StatelessWidget {
  const ListMoviePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ListMovieCubit>(
      create: (context) {
        final moviesRepo = RepositoryProvider.of<MoviesRepository>(context);
        return ListMovieCubit(
          moviesRepo: moviesRepo,
        );
      },
      child: const ListMovieChildPage(),
    );
  }
}

class ListMovieChildPage extends StatefulWidget {
  const ListMovieChildPage({Key? key}) : super(key: key);

  @override
  State<ListMovieChildPage> createState() => _ListMovieChildPageState();
}

class _ListMovieChildPageState extends State<ListMovieChildPage>
    with AutomaticKeepAliveClientMixin {
  late ListMovieCubit _listMovieCubit;
  late int indexPopularMovie = 0;
  late int indexUpcomingMovie = 0;

  @override
  void initState() {
    super.initState();
    final movieRepo = RepositoryProvider.of<MoviesRepository>(context);
    _listMovieCubit = ListMovieCubit(moviesRepo: movieRepo);
    _listMovieCubit.initData();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: SingleChildScrollView(
        child: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
                colors: AppColors.colorBackground,
                begin: Alignment.centerLeft,
                end: Alignment.centerRight),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const NotificationWidget(),
              const SizedBox(
                height: 20,
              ),
              const SearchBarWidget(),
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 50),
                child: TextWidget(
                  text: 'Most Popular',
                  textStyle: AppTextStyle.whiteS18W700,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              _mostPopularMovie(),
              const SizedBox(height: 15),
              const ListButtonAppWidget(),
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 50),
                child: TextWidget(
                  text: 'Upcoming releases',
                  textStyle: AppTextStyle.whiteS18W700,
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              _upcomingMovie(),
              const SizedBox(
                height: 30,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _upcomingMovie() {
    return BlocBuilder<ListMovieCubit, ListMovieState>(
      bloc: _listMovieCubit,
      buildWhen: (previous, current) =>
          previous.loadUpcomingMovieStatus != current.loadUpcomingMovieStatus ||
          previous.selectedIndexUpcoming != current.selectedIndexUpcoming,
      builder: (context, state) {
        return state.loadUpcomingMovieStatus == StatusEnum.success
            ? UpcomingMovieWidget(
                onIndexChanged: (index) {
                  _listMovieCubit.switchUpcomingDotIndicators(index);
                  indexUpcomingMovie = index;
                },
                itemCount: state.upcomingMovieEntity?.results?.length ?? 0,
                position: state.selectedIndexUpcoming?.toDouble() ?? 0,
                upcomingMovieEntity:
                    state.upcomingMovieEntity ?? UpcomingMovieEntity(),
              )
            : SizedBox(
                height: 0.25 * MediaQuery.of(context).size.height,
                child: const Center(
                  child: AppCircularProgressIndicator(),
                ),
              );
      },
    );
  }

  Widget _mostPopularMovie() {
    return BlocBuilder<ListMovieCubit, ListMovieState>(
      bloc: _listMovieCubit,
      buildWhen: (previous, current) =>
          previous.loadPopularMovieStatus != current.loadPopularMovieStatus ||
          previous.selectedIndexMostPopular != current.selectedIndexMostPopular,
      builder: (context, state) {
        return state.loadPopularMovieStatus == StatusEnum.success
            ? MostPopularMovieWidget(
                onIndexChanged: (index) {
                  _listMovieCubit.switchMostPopularDotIndicators(index);
                  indexPopularMovie = index;
                },
                mostPopularMovieEntity:
                    state.mostPopularMovieEntity ?? MostPopularMovieEntity(),
                itemCount: state.mostPopularMovieEntity?.results?.length ?? 0,
                position: state.selectedIndexMostPopular?.toDouble() ?? 0,
              )
            : SizedBox(
                height: 0.25 * MediaQuery.of(context).size.height,
                child: const Center(
                  child: AppCircularProgressIndicator(),
                ),
              );
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
