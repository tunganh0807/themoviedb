import 'package:flutter/material.dart';
import 'package:the_movie_db/models/entity/movie_entity/result_info_movie_entity.dart';
import 'package:the_movie_db/ui/components/app_cache_image.dart';
import 'package:the_movie_db/ui/pages/detail_film_page/detail_movie_page.dart';

class ItemUpcomingMovie extends StatelessWidget {
  final ResultInfoMovieEntity resultInfoMovieEntity;

  const ItemUpcomingMovie({
    Key? key,
    required this.resultInfoMovieEntity,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => DetailMoviePage(
              movieID: resultInfoMovieEntity.id.toString(),
            ),
          ),
        );
      },
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
        ),
        child: AspectRatio(
          aspectRatio: 145 / 215,
          child: AppCacheImage(
            url: resultInfoMovieEntity.posterUrl,
            connerRadius: 30,
          ),
        ),
      ),
    );
  }
}
