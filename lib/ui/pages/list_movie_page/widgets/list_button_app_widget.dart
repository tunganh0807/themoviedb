import 'package:flutter/material.dart';
import 'package:the_movie_db/common/app_button_list_movie_page.dart';
import 'package:the_movie_db/models/entity/button_movie_page/button_movie_page.dart';

import 'package:the_movie_db/ui/components/button_app_widget.dart';

class ListButtonAppWidget extends StatelessWidget {
  const ListButtonAppWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 50),
      child: SizedBox(
        height: MediaQuery.of(context).size.height*0.12,
        child: ListView.separated(
          separatorBuilder: (context, index) => const SizedBox(
            width: 10,
          ),
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: 4,
          itemBuilder: (BuildContext context, int index) {
            return _itemListButton(
              AppButtonListMoviePage.listButton[index],
            );
          },
        ),
      ),
    );
  }

  Widget _itemListButton(ButtonMoviePage buttonMoviePage) {
    return ButtonAppWidget(
      image: buttonMoviePage.image,
      buttonName: buttonMoviePage.buttonName,
    );
  }
}
