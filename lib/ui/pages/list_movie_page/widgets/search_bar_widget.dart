import 'package:flutter/material.dart';
import 'package:the_movie_db/common/app_colors.dart';
import 'package:the_movie_db/common/app_images.dart';
import 'package:the_movie_db/common/app_text_style.dart';

class SearchBarWidget extends StatelessWidget {
  const SearchBarWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 50),
      padding: const EdgeInsets.all(15),
      height: 50,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        border: Border.all(
          color: AppColors.colorBorderSearchBar,
        ),
        gradient: const LinearGradient(
          colors: AppColors.colorBackgroundSearchBar,
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
        ),
      ),
      child: Row(
        children: [
          Expanded(
            child: TextFormField(
              textAlignVertical: TextAlignVertical.center,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(top: 5),
                hintText: 'Search',
                hintStyle: AppTextStyle.whiteS18W400,
                border: const OutlineInputBorder(
                  borderSide: BorderSide.none,
                ),
                prefixIcon: Image.asset(AppImages.icSearch),
              ),
            ),
          ),
          Container(
            height: 35,
            width: 1,
            color: AppColors.colorBorderSearchBar,
          ),
          const SizedBox(
            width: 15,
          ),
          Image.asset(
            AppImages.icMicro,
            height: 20,
            width: 15,
          ),
        ],
      ),
    );
  }
}
