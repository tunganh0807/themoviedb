import 'package:flutter/material.dart';
import 'package:the_movie_db/models/entity/movie_entity/result_info_movie_entity.dart';
import 'package:the_movie_db/ui/components/app_cache_image.dart';
import 'package:the_movie_db/ui/pages/detail_film_page/detail_movie_page.dart';

class ItemPopularMovie extends StatelessWidget {
  final ResultInfoMovieEntity resultInfoMovieEntity;

  const ItemPopularMovie({
    Key? key,
    required this.resultInfoMovieEntity,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DetailMoviePage(
              movieID: resultInfoMovieEntity.id.toString(),
            ),
          ),
        );
      },
      child: AppCacheImage(
        connerRadius: 30,
        url: resultInfoMovieEntity.backdropUrl,
      ),
    );
  }
}
