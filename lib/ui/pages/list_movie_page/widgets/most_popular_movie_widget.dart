import 'package:flutter/material.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';
import 'package:the_movie_db/models/entity/movie_entity/most_popular_movie_entity.dart';
import 'package:the_movie_db/models/entity/movie_entity/result_info_movie_entity.dart';
import 'package:the_movie_db/ui/components/dot_indicator_widget.dart';
import 'package:the_movie_db/ui/pages/list_movie_page/widgets/item_popular_movie.dart';

class MostPopularMovieWidget extends StatelessWidget {
  final ValueChanged<int> onIndexChanged;
  final MostPopularMovieEntity mostPopularMovieEntity;
  final int itemCount;
  final double position;

  const MostPopularMovieWidget({
    Key? key,
    required this.onIndexChanged,
    required this.mostPopularMovieEntity,
    required this.itemCount,
    required this.position,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 0.15 * MediaQuery.of(context).size.height,
          child: Swiper(
            onIndexChanged: (index) {
              onIndexChanged.call(index);
            },
            loop: true,
            fade: 0.3,
            autoplay: true,
            viewportFraction: 0.8,
            scale: 0.8,
            itemBuilder: (BuildContext context, int index) {
              return ItemPopularMovie(
                resultInfoMovieEntity: mostPopularMovieEntity.results?[index] ?? ResultInfoMovieEntity(),
              );
            },
            itemCount: itemCount,
          ),
        ),
        const SizedBox(
          height: 15,
        ),
        Center(
          child: DotIndicatorWidget(
            dotsCount: itemCount,
            size: 9,
            position: position,
            spacing: 3,
          ),
        ),
      ],
    );
  }
}
