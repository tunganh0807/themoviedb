import 'package:flutter/material.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';
import 'package:the_movie_db/models/entity/movie_entity/result_info_movie_entity.dart';
import 'package:the_movie_db/models/entity/movie_entity/upcoming_movie_entity.dart';
import 'package:the_movie_db/ui/components/dot_indicator_widget.dart';
import 'package:the_movie_db/ui/pages/list_movie_page/widgets/item_upcoming_movie.dart';

class UpcomingMovieWidget extends StatelessWidget {
  final ValueChanged<int> onIndexChanged;
  final UpcomingMovieEntity upcomingMovieEntity;
  final int itemCount;
  final double position;

  const UpcomingMovieWidget({
    Key? key,
    required this.onIndexChanged,
    required this.upcomingMovieEntity,
    required this.itemCount,
    required this.position,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 0.23 * MediaQuery.of(context).size.height,
          child: Swiper(
            scrollDirection: Axis.horizontal,
            loop: true,
            onIndexChanged: (index) {
              onIndexChanged.call(index);
            },
            autoplay: true,
            fade: 0.3,
            viewportFraction: 0.45,
            itemBuilder: (BuildContext context, int index) {
              return ItemUpcomingMovie(
                resultInfoMovieEntity: upcomingMovieEntity.results?[index] ??
                    ResultInfoMovieEntity(),
              );
            },
            itemCount: itemCount,
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        Center(
          child: DotIndicatorWidget(
            dotsCount: itemCount,
            size: 9,
            position: position,
            spacing: 3,
          ),
        ),
      ],
    );
  }
}
