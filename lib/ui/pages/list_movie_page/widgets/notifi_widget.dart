import 'package:flutter/material.dart';
import 'package:the_movie_db/common/app_images.dart';
import 'package:the_movie_db/common/app_text_style.dart';
import 'package:the_movie_db/ui/components/text_widget.dart';

class NotificationWidget extends StatelessWidget {
  const NotificationWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 65,
        right: 65,
        left: 65,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          TextWidget(
            text: 'Hello, Jane!',
            textStyle: AppTextStyle.whiteS18W500,
          ),
          Image.asset(
            AppImages.icBell,
            height: 25,
            width: 25,
          ),
        ],
      ),
    );
  }
}
