import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:the_movie_db/models/enum/status_enum.dart';
import 'package:the_movie_db/repositories/credits_repository.dart';
import 'package:the_movie_db/repositories/movies_repository.dart';
import 'package:the_movie_db/ui/pages/detail_film_page/detail_movie_state.dart';

class DetailMovieCubit extends Cubit<DetailMovieState> {
  MoviesRepository moviesRepository;
  CreditsRepository creditsRepository;

  DetailMovieCubit({
    required this.moviesRepository,
    required this.creditsRepository,
  }) : super(const DetailMovieState());

  Future<void> getDetailFilm(String movieID) async {
    try {
      emit(
        state.copyWith(
          loadDetailFilmStatus: StatusEnum.loading,
        ),
      );
      final result = await moviesRepository.getDetailMovie(
        movieID: int.parse(movieID),
      );
      emit(
        state.copyWith(
          detailMovieEntity: result,
          loadDetailFilmStatus: StatusEnum.success,
        ),
      );
    } catch (e) {
      emit(
        state.copyWith(loadDetailFilmStatus: StatusEnum.fail),
      );
      debugPrint(e.toString());
    }
  }

  Future<void> getCreditsFilm(String movieID) async {
    try {
      emit(
        state.copyWith(
          loadCreditsFilmStatus: StatusEnum.loading,
        ),
      );
      final result = await creditsRepository.getActor(
        movieID: int.parse(movieID),
      );
      emit(
        state.copyWith(
          loadCreditsFilmStatus: StatusEnum.success,
          creditsEntity: result,
        ),
      );
    } catch (e) {
      emit(
        state.copyWith(
          loadCreditsFilmStatus: StatusEnum.fail,
        ),
      );
      debugPrint(e.toString());
    }
  }
}
