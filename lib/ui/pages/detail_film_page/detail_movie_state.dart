import 'package:equatable/equatable.dart';
import 'package:the_movie_db/models/entity/credits_entity/credits_entity.dart';
import 'package:the_movie_db/models/entity/movie_entity/detail_movie_entity.dart';
import 'package:the_movie_db/models/enum/status_enum.dart';

class DetailMovieState extends Equatable {
  final DetailMovieEntity? detailMovieEntity;
  final CreditsEntity? creditsEntity;
  final StatusEnum? loadCreditsFilmStatus;
  final StatusEnum? loadDetailFilmStatus;

  const DetailMovieState({
    this.loadDetailFilmStatus,
    this.detailMovieEntity,
    this.creditsEntity,
    this.loadCreditsFilmStatus,
  });

  @override
  List<Object?> get props => [
        detailMovieEntity,
        creditsEntity,
        loadCreditsFilmStatus,
        loadDetailFilmStatus,
      ];

  DetailMovieState copyWith({
    DetailMovieEntity? detailMovieEntity,
    CreditsEntity? creditsEntity,
    StatusEnum? loadCreditsFilmStatus,
    StatusEnum? loadDetailFilmStatus,
  }) {
    return DetailMovieState(
      detailMovieEntity: detailMovieEntity ?? this.detailMovieEntity,
      creditsEntity: creditsEntity ?? this.creditsEntity,
      loadCreditsFilmStatus:
          loadCreditsFilmStatus ?? this.loadCreditsFilmStatus,
      loadDetailFilmStatus: loadDetailFilmStatus ?? this.loadDetailFilmStatus,
    );
  }
}
