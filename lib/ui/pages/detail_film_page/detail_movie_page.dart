import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:readmore/readmore.dart';
import 'package:the_movie_db/common/app_colors.dart';
import 'package:the_movie_db/common/app_images.dart';
import 'package:the_movie_db/common/app_text_style.dart';
import 'package:the_movie_db/models/entity/credits_entity/cast_entity/cast_entity.dart';
import 'package:the_movie_db/models/entity/movie_entity/genre_entity.dart';
import 'package:the_movie_db/models/enum/status_enum.dart';
import 'package:the_movie_db/repositories/credits_repository.dart';
import 'package:the_movie_db/repositories/movies_repository.dart';
import 'package:the_movie_db/ui/components/actor_widget.dart';
import 'package:the_movie_db/ui/components/app_cache_image.dart';
import 'package:the_movie_db/ui/components/app_circular_progress_indicator.dart';
import 'package:the_movie_db/ui/components/text_widget.dart';
import 'package:the_movie_db/ui/pages/detail_film_page/detail_movie_cubit.dart';
import 'package:the_movie_db/ui/pages/detail_film_page/detail_movie_state.dart';

class DetailMoviePage extends StatelessWidget {
  final String movieID;

  const DetailMoviePage({
    Key? key,
    required this.movieID,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) {
        final moviesRepo = RepositoryProvider.of<MoviesRepository>(context);
        final creditRepo = RepositoryProvider.of<CreditsRepository>(context);
        return DetailMovieCubit(
          moviesRepository: moviesRepo,
          creditsRepository: creditRepo,
        );
      },
      child: DetailMovieChildPage(movieID: movieID),
    );
  }
}

class DetailMovieChildPage extends StatefulWidget {
  final String movieID;

  const DetailMovieChildPage({
    super.key,
    required this.movieID,
  });

  @override
  State<DetailMovieChildPage> createState() => _DetailMovieChildPageState();
}

class _DetailMovieChildPageState extends State<DetailMovieChildPage> {
  late DetailMovieCubit _detailFilmCubit;

  @override
  void initState() {
    super.initState();
    final movieRepo = RepositoryProvider.of<MoviesRepository>(context);
    final creditRepo = RepositoryProvider.of<CreditsRepository>(context);
    _detailFilmCubit = DetailMovieCubit(
      creditsRepository: creditRepo,
      moviesRepository: movieRepo,
    );
    _detailFilmCubit.getDetailFilm(widget.movieID);
    _detailFilmCubit.getCreditsFilm(widget.movieID);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            child: _imageBackground(),
          ),
          _bottomInfo(),
          Positioned(
            top: 55,
            left: 50,
            child: _iconBack(),
          ),
        ],
      ),
    );
  }

  Widget _bottomInfo() {
    return SingleChildScrollView(
      child: BlocBuilder<DetailMovieCubit, DetailMovieState>(
        bloc: _detailFilmCubit,
        buildWhen: (previous, current) =>
            previous.loadDetailFilmStatus != current.loadDetailFilmStatus,
        builder: (context, state) {
          return Container(
            padding: const EdgeInsets.symmetric(horizontal: 50),
            margin:
                EdgeInsets.only(top: MediaQuery.of(context).size.height / 1.6),
            constraints: BoxConstraints(
              minHeight: MediaQuery.of(context).size.height / 2.6,
            ),
            decoration: BoxDecoration(
              gradient: const LinearGradient(
                  colors: AppColors.colorBackground,
                  end: Alignment.centerRight,
                  begin: Alignment.centerLeft),
              border: Border.all(
                color: const Color(0x4DFFFFFF),
              ),
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(50),
                topRight: Radius.circular(50),
              ),
            ),
            child: Column(
              children: [
                const SizedBox(
                  height: 15,
                ),
                TextWidget(
                  text: state.detailMovieEntity?.title ?? '',
                  textStyle: AppTextStyle.whiteS18W700,
                ),
                const SizedBox(
                  height: 30,
                ),
                _infoView(),
                const SizedBox(
                  height: 20,
                ),
                _filmDescription(),
                const SizedBox(
                  height: 20,
                ),
                _actor(),
              ],
            ),
          );
        },
      ),
    );
  }

  Widget _iconBack() {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: Image.asset(
        AppImages.icBack,
        height: 25,
        width: 25,
      ),
    );
  }

  Widget _imageBackground() {
    return BlocBuilder<DetailMovieCubit, DetailMovieState>(
      bloc: _detailFilmCubit,
      buildWhen: (previous, current) =>
          previous.loadDetailFilmStatus != current.loadDetailFilmStatus,
      builder: (context, state) {
        return state.loadDetailFilmStatus == StatusEnum.success
            ? AppCacheImage(
                url: state.detailMovieEntity?.posterUrl ?? '',
                width: MediaQuery.of(context).size.width,
                height: 0.7 * MediaQuery.of(context).size.height,
              )
            : const Center(
                child: AppCircularProgressIndicator(),
              );
      },
    );
  }

  Widget _actor() {
    return Column(children: [
      Row(
        children: [
          Expanded(
              child: TextWidget(
            text: 'Cast',
            textStyle: AppTextStyle.whiteS18W700,
          )),
          TextWidget(
            text: 'See All',
            textStyle: AppTextStyle.whiteS15W500,
          ),
        ],
      ),
      const SizedBox(
        height: 15,
      ),
      SizedBox(
        height: 100,
        child: BlocBuilder<DetailMovieCubit, DetailMovieState>(
          bloc: _detailFilmCubit,
          buildWhen: (previous, current) =>
              previous.loadCreditsFilmStatus != current.loadCreditsFilmStatus,
          builder: (context, state) {
            return ListView.separated(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              separatorBuilder: (context, index) => const SizedBox(
                width: 20,
              ),
              itemCount: 5,
              itemBuilder: (BuildContext context, int index) {
                return _itemActor(
                  state.creditsEntity?.cast?[index] ?? CastEntity(),
                );
              },
            );
          },
        ),
      )
    ]);
  }

  Widget _itemActor(CastEntity castEntity) {
    return ActorWidget(
      url: castEntity.getProfilePath,
      name: castEntity.name ?? '',
      nameCharacter: castEntity.character ?? '',
    );
  }

  Widget _filmDescription() {
    return BlocBuilder<DetailMovieCubit, DetailMovieState>(
      bloc: _detailFilmCubit,
      buildWhen: (previous, current) =>
          previous.loadDetailFilmStatus != current.loadDetailFilmStatus,
      builder: (context, state) {
        return ReadMoreText(
          state.detailMovieEntity?.overview ?? 'No Description',
          trimLines: 3,
          colorClickableText: Colors.blue,
          textAlign: TextAlign.justify,
          style: AppTextStyle.whiteS12W500,
          trimMode: TrimMode.Line,
          trimCollapsedText: 'More',
          trimExpandedText: 'Less',
          moreStyle: AppTextStyle.blueS12W500,
        );
      },
    );
  }

  Widget _infoView() {
    return BlocBuilder<DetailMovieCubit, DetailMovieState>(
      bloc: _detailFilmCubit,
      buildWhen: (previous, current) =>
          previous.loadDetailFilmStatus != current.loadDetailFilmStatus,
      builder: (context, state) {
        return Row(
          children: [
            SizedBox(
              height: 25,
              child: ListView.separated(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: 1,
                itemBuilder: (context, index) => _itemGenres(
                  state.detailMovieEntity?.genres?[index] ??
                      GenreEntity(name: 'Loading'),
                ),
                separatorBuilder: (context, index) => const SizedBox(width: 5),
              ),
            ),
            const SizedBox(
              width: 5,
            ),
            Container(
              height: 25,
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: AppColors.colorYellow,
              ),
              child: Center(
                child: TextWidget(
                  text: state.detailMovieEntity?.voteAverage != null
                      ? 'IMDB ${(state.detailMovieEntity?.voteAverage).toString().substring(0, 3)}'
                      : 'Loading',
                  textStyle: AppTextStyle.blackS12W700,
                ),
              ),
            ),
            const Expanded(
              child: SizedBox(),
            ),
            Image.asset(
              AppImages.icShare,
              width: 20,
              height: 25,
            ),
            const SizedBox(
              width: 10,
            ),
            Image.asset(
              AppImages.icFavorite,
              width: 25,
              height: 25,
            )
          ],
        );
      },
    );
  }

  Widget _itemGenres(GenreEntity genreEntity) {
    return Container(
      padding: const EdgeInsets.all(5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        gradient: const LinearGradient(
          colors: AppColors.colorBackgroundGenre,
          end: Alignment.centerRight,
          begin: Alignment.centerLeft,
        ),
      ),
      child: TextWidget(
        text: genreEntity.name ?? '',
        textStyle: AppTextStyle.whiteS12W700,
      ),
    );
  }
}
