import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:the_movie_db/common/app_colors.dart';
import 'package:the_movie_db/configs/app_config.dart';
import 'package:the_movie_db/ui/pages/home_page/home_cubit.dart';
import 'package:the_movie_db/ui/pages/home_page/home_state.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<HomeCubit>(
      create: (context) {
        return HomeCubit();
      },
      child: const HomeChildPage(),
    );
  }
}

class HomeChildPage extends StatefulWidget {
  const HomeChildPage({Key? key}) : super(key: key);

  @override
  State<HomeChildPage> createState() => _HomeChildPageState();
}

class _HomeChildPageState extends State<HomeChildPage> {
  late HomeCubit _homeCubit;
  final PageController _pageController = PageController();

  @override
  void initState() {
    super.initState();
    _homeCubit = HomeCubit();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BlocConsumer<HomeCubit, HomeState>(
        bloc: _homeCubit,
        listenWhen: (previous, current) =>
            previous.selectedIndex != current.selectedIndex,
        listener: (context, state) {
          _pageController.jumpToPage(state.selectedIndex);
        },
        buildWhen: (previous, current) {
          return previous.selectedIndex != current.selectedIndex;
        },
        builder: (context, state) {
          return CurvedNavigationBar(
            onTap: (index) {
              _homeCubit.switchPage(index);
            },
            backgroundColor: AppColors.colorBackgroundBottomNavigationBar,
            height: 60,
            index: state.selectedIndex,
            items: AppConfig.listItemBottomNavigationBar,
          );
        },
      ),
      body: PageView(
        onPageChanged: (index) {
          _homeCubit.switchPage(index);
        },
        controller: _pageController,
        children: AppConfig.listItemPageView,
      ),
    );
  }
}
