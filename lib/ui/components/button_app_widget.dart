import 'package:flutter/material.dart';
import 'package:the_movie_db/common/app_colors.dart';
import 'package:the_movie_db/common/app_text_style.dart';

class ButtonAppWidget extends StatelessWidget {
  final String image, buttonName;

  const ButtonAppWidget({
    Key? key,
    required this.image,
    required this.buttonName,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: (MediaQuery.of(context).size.width - 130) / 4,
      padding: const EdgeInsets.only(left: 10, right: 10, bottom: 10, top: 15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        border: Border.all(
          color: AppColors.colorBorderSearchBar,
        ),
        gradient: const LinearGradient(
          colors: AppColors.colorBackgroundButtonMoviePage,
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Flexible(
            child: Image.asset(
              image,
              height: 30,
              width: 30,
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Expanded(
            child: Text(
              buttonName,
              textAlign: TextAlign.center,
              style: AppTextStyle.whiteS9W400,
            ),
          ),
        ],
      ),
    );
  }
}
