import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:the_movie_db/network/api_client.dart';
import 'package:the_movie_db/network/api_util.dart';
import 'package:the_movie_db/repositories/credits_repository.dart';
import 'package:the_movie_db/repositories/movies_repository.dart';
import 'package:the_movie_db/ui/pages/home_page/home_page.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late ApiClient _apiClient;

  @override
  void initState() {
    super.initState();
    _apiClient = ApiUtil.apiClient;
  }

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<CreditsRepository>(create: (context) {
          return CreditsRepositoryImpl(apiClient: _apiClient);
        }),
        RepositoryProvider<MoviesRepository>(create: (context) {
          return MoviesRepositoryImpl(apiClient: _apiClient);
        }),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const HomePage(),
      ),
    );
  }
}
