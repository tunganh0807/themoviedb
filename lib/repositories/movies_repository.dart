import 'package:the_movie_db/configs/app_config.dart';
import 'package:the_movie_db/models/entity/movie_entity/detail_movie_entity.dart';
import 'package:the_movie_db/models/entity/movie_entity/most_popular_movie_entity.dart';
import 'package:the_movie_db/models/entity/movie_entity/upcoming_movie_entity.dart';
import 'package:the_movie_db/network/api_client.dart';

abstract class MoviesRepository {
  Future<MostPopularMovieEntity?> getPopularMovies({required int page});

  Future<UpcomingMovieEntity?> getUpcomingMovies({required int page});

  Future<DetailMovieEntity?> getDetailMovie({required int movieID});
}

class MoviesRepositoryImpl extends MoviesRepository {
  ApiClient apiClient;

  MoviesRepositoryImpl({
    required this.apiClient,
  });

  @override
  Future<MostPopularMovieEntity?> getPopularMovies({required int page}) {
    return apiClient.getPopularMovies(
      AppConfig.apiKey,
      page,
    );
  }

  @override
  Future<UpcomingMovieEntity?> getUpcomingMovies({required int page}) {
    return apiClient.getUpcomingMovies(
      AppConfig.apiKey,
      page,
    );
  }

  @override
  Future<DetailMovieEntity?> getDetailMovie({required int movieID}) {
    return apiClient.getDetailMovies(
      movieID.toString(),
      AppConfig.apiKey,
    );
  }
}
