import 'package:the_movie_db/configs/app_config.dart';
import 'package:the_movie_db/models/entity/credits_entity/credits_entity.dart';
import 'package:the_movie_db/network/api_client.dart';

abstract class CreditsRepository {
  Future<CreditsEntity?> getActor({required int movieID});
}

class CreditsRepositoryImpl extends CreditsRepository {
  ApiClient apiClient;

  CreditsRepositoryImpl({
    required this.apiClient,
  });

  @override
  Future<CreditsEntity?> getActor({required int movieID}) {
    return apiClient.getActor(
      movieID.toString(),
      AppConfig.apiKey,
    );
  }
}
